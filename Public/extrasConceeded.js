//calculating starting and ending match id of 2016
const fs = require('fs');
const parse = require('csv-parse');

let startMatch = 0;
let endMatch = 0;
const parser = parse({
    columns: true
}, function (err, file) {
    for (let record = 0; record < file.length; record++) {
        if (parseInt(file[record]['season']) == '2016' && startMatch == 0) {
            startMatch = parseInt(file[record]['id']);
            endMatch = parseInt(file[record]['id']);
        } else if (parseInt(file[record]['season']) == '2016') {
            endMatch++;
        }
    }
});
fs.createReadStream('./../../Data/matches.csv').pipe(parser);

//calculate extra runs conceeded by each team in each match 
const parser1 = parse({
    columns: true
}, function (err, file) {
    let startingPoint = 0;
    let currTeam;
    let runsObj = {};
    for (startingPoint = startMatch * 200; startingPoint < file.length && parseInt(file[startingPoint]['match_id']) !== startMatch; startingPoint++) {}

    while (startingPoint < file.length && parseInt(file[startingPoint]['match_id']) <= endMatch) {
        currTeam = file[startingPoint]['bowling_team'];
        let runs = 0;
        while (startingPoint < file.length && parseInt(file[startingPoint]['match_id']) <= endMatch && currTeam == file[startingPoint]['bowling_team']) {
            runs += parseInt(file[startingPoint]['extra_runs']);
            startingPoint++;
        }
        if (!runsObj.hasOwnProperty(currTeam)) {
            runsObj[currTeam] = runs;
        } else {
            runsObj[currTeam] += runs;
        }
    }
    console.log(runsObj);
})
fs.createReadStream('./../../Data/deliveries.csv').pipe(parser1);

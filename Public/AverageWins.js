//Calculating Total number of Wins of all teams

const fs = require('fs');
const parse = require('csv-parse');

const parser = parse({
    columns: true
}, function (err, file) {

    let objWinsNumber = {};

    for (let record = 0; record < file.length; record++) {

        if (objWinsNumber.hasOwnProperty(file[record]['winner'])) {
            str = file[record]['winner'];
            objWinsNumber[str] = objWinsNumber[str] + 1;
        } else {
            const winner = file[record]['winner'];
            objWinsNumber[winner] = 1;

        }
    }

    //calculating total number of seasons played by all the teams 
    let objSeasonsPlayed = {};
    let SeasonsPlayed = {};

    for (let record = 0; record < file.length;) {
        let year = file[record]['season'];
        let winner;

        for (; record < file.length && year == parseInt(file[record]['season']); record++) {
            if (objSeasonsPlayed.hasOwnProperty(file[record]['winner']) == false) {
                winner = file[record]['winner'];
                objSeasonsPlayed[winner] = 1;
            }
        }

        const objectkeys = Object.keys(objSeasonsPlayed);

        for (let itr = 0; itr < objectkeys.length; itr++) {
            if (SeasonsPlayed.hasOwnProperty(objectkeys[itr])) {
                SeasonsPlayed[objectkeys[itr]]++;
            } else {
                SeasonsPlayed[objectkeys[itr]] = 1;
            }
        }
        objSeasonsPlayed = {};
    }

    //Calculating wins per season

    const arraykey = Object.keys(SeasonsPlayed);

    for (let team = 0; team < arraykey.length; team++) {
        objWinsNumber[arraykey[team]] = objWinsNumber[arraykey[team]] / SeasonsPlayed[arraykey[team]];
    }

    console.log(objWinsNumber);
});
fs.createReadStream('./../../Data/matches.csv').pipe(parser);

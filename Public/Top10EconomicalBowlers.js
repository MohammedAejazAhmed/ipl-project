//Find Starting and ending matches of year 2015

const fs = require('fs');
const parse = require('csv-parse');

let startMatch = 0;
let endMatch = 0;

const parser = parse({
    columns: true
}, function (err, file) {

    for (let record = 0; record < file.length; record++) {

        if (parseInt(file[record]['season']) == '2015' && startMatch == 0) {
            startMatch = parseInt(file[record]['id']);
            endMatch = parseInt(file[record]['id']);
        } else if (parseInt(file[record]['season']) == '2015') {
            endMatch++;
        }
    }
});
fs.createReadStream('./../../Data/matches.csv').pipe(parser);

const parser1 = parse({
    columns: true
}, function (err, file) {

    //store values of all the bowlers and the runs they consceeded

    let startingPoint = 0;
    let currBowler;
    let runsObj = {};
    let bowlsObj = {};

    for (startingPoint = startMatch * 200; parseInt(file[startingPoint]['match_id']) !== startMatch; startingPoint++) {}

    while (parseInt(file[startingPoint]['match_id']) <= endMatch) {

        currBowler = file[startingPoint]['bowler'];
        let runs = 0;
        let bowls = 0

        while (parseInt(file[startingPoint]['match_id']) <= endMatch && currBowler == file[startingPoint]['bowler']) {

            runs += parseInt(file[startingPoint]['total_runs']);
            startingPoint++;
            bowls++;
        }

        if (!runsObj.hasOwnProperty(currBowler)) {
            runsObj[currBowler] = runs;
            bowlsObj[currBowler] = bowls;
        } else {
            runsObj[currBowler] += runs;
            bowlsObj[currBowler] += bowls;
        }
    }

    //Find Economies of the Bowlers

    const arraykey = Object.keys(runsObj);

    for (let player = 0; player < arraykey.length; player++) {
        runsObj[arraykey[player]] = runsObj[arraykey[player]] / bowlsObj[arraykey[player]] * 6;
    }

    //Sort all the bowlers by their economies

    let Sort = [];
    for (let player in runsObj) {
        Sort.push([player, runsObj[player]]);
    }
    Sort.sort(function (a, b) {
        return a[1] - b[1];
    });

    for (let player = 0; player < 9; player++) {
        console.log(Sort[player]);
    }
})
fs.createReadStream('./../../Data/deliveries.csv').pipe(parser1);